ORG 0000H
LJMP    MAIN
ORG 0003H
LJMP    START
ORG 000BH
DJNZ R5,$+3
CPL P3.1
MOV R5,#200
RETI
ORG 001BH
LJMP    PROCESS
ORG 0030H

MAIN:   MOV P0,#0C0H  ;   显示器个位
        MOV P1,#0C0H  ;   显示器十位
        MOV TMOD,#12    ;   T1模式一定时，T0模式二定时
        MOV TL0,#(256-250)
        MOV TH0,#(256-250)
        MOV TL1,#LOW(65536-50000)   ;   T1 50ms 定时
        MOV TH1,#HIGH(65536-50000)
        MOV R7,#20  ;   定时1s
        MOV R6,#60  ;   定时1min
        MOV R5,#200   ;   周期为50ms的蜂鸣器
        MOV TCON,#41    ;启动定时器1 INT0采用下跳沿中断
        MOV IE,#19  ;   开EA ET1 EX0
        MOV IP,#01
        AJMP $

START:  JB  P3.0,CLRP3  ;   若是工作状态则跳转至清零程序 
        MOV A,P2
        MOV 40H,A
        ACALL DIS
        CLR P3.0
        RETI

CLRP3:  MOV P0,#0C0H
        MOV P1,#0C0H
        MOV TL1,#LOW(65536-50000)   ;   T1 50ms 定时
        MOV TH1,#HIGH(65536-50000)
        MOV R7,#20  ;   定时1s
        MOV R6,#60  ;   定时1min
        CLR P3.0
        CLR P3.1
        CLR TR0
        CLR ET0

PROCESS: DJNZ R7,X
         DJNZ R6,X
         MOV TL1,#LOW(65536-50000)   ;   T1 50ms 定时
         MOV TH1,#HIGH(65536-50000)
         MOV R7,#20
         MOV R6,#60
         MOV A,40H  ;   这个地方怎么写来者?
         DEC A 
         DA A
         MOV 40H,A
         AJMP DIS
         MOV A,40H
         ACALL SOUND  ;   等于零时响铃
X:       RET

SOUND:   SETB TR0
         SETB ET0
         RET

DIS:     MOV A,40H
         ANL A,#0FH   ;   个位分钟BCD码 
         ACALL PTAB
         MOV P0,A 
         MOV A,40H
         SWAP A
         ANL A,#0FH   ;   十位分钟BCD码
         ACALL PTAB
         MOV P1,A
         RET
PTAB:    INC A
         MOVC A,@A+PC
         RET
DB       0C0H,0F9H,0A4H,0B0H,99H,92H,82H,0F8H
DB       90H,88H,83H,0C6H,0A1H,86H,8EH,80H

END